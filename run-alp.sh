#!/bin/sh
$HOME/alp ltsv --filters "Uri matches '^/api/.+$'" --file /var/log/nginx/access.log \
  -m "\
  ^/initialize$,\
  ^/login$,\
  ^/logout$,\
  ^/api/users/me/courses$,\
  ^/api/users/me/grades$,\
  ^/api/users/me$,\
  ^/api/courses/.+/classes/.+/assignments/export$,\
  ^/api/courses/.+/classes/.+/assignments/scores$,\
  ^/api/courses/.+/classes/.+/assignments$,\
  ^/api/courses/.+/classes$,\
  ^/api/courses/.+/status$,\
  ^/api/courses/.+$,\
  ^/api/courses$,\
  ^/api/announcements/.+$,\
  ^/api/announcements$\
  "